import random
import numpy as np
import pygame

pygame.font.init()
screen = pygame.display.set_mode((500,600))
pygame.display.set_caption("SUDOKU")

# Fonty
font1 = pygame.font.SysFont("arial",40)
font2 = pygame.font.SysFont("arial",25)
font3 = pygame.font.SysFont("arial",30)

# Barvy
green = (102, 255, 102)
orange = (255, 204, 0)
red = (223, 52, 52)

# Vytvoreni menu pro vyber obtiznosti sudoku
def menu():
    text = font1.render("Vyberte obtížnost",1,(0,0,0)) 
    screen.blit(text,(text.get_rect(center=(250,80))))
    pygame.draw.rect(screen,green,[170,160,160,50],0,15)
    pygame.draw.rect(screen,(0,0,0),[170,160,160,50],2,15)
    pygame.draw.rect(screen,orange,[170,250,160,50],0,15)
    pygame.draw.rect(screen,(0,0,0),[170,250,160,50],2,15)
    pygame.draw.rect(screen,red,[170,340,160,50],0,15)
    pygame.draw.rect(screen,(0,0,0),[170,340,160,50],2,15)
    text = font3.render("Lehká",1,(0,0,0))
    screen.blit(text,(text.get_rect(center=(250,185))))
    text = font3.render("Střední",1,(0,0,0))
    screen.blit(text,(text.get_rect(center=(250,275))))
    text = font3.render("Těžká",1,(0,0,0))
    screen.blit(text,(text.get_rect(center=(250,365))))
    text = font1.render("Hodně štěstí!",1,(0,0,0))
    screen.blit(text,(text.get_rect(center=(250,500))))
    pygame.draw.rect(screen,(0,0,0),[140,130,220,290],4)

# Nastaveni obtiznosti
def click(pos):
    k = 0
    if 170 <= pos[0] <= 330 and 160 <= pos[1] <= 210:
        k = 30
    if 170 <= pos[0] <= 330 and 250 <= pos[1] <= 300:
        k = 35
    if 170 <= pos[0] <= 330 and 340 <= pos[1] <= 390:
        k = 40
    return k

# Okno menu + vyber obtiznosti
k = 0
run = True
while k == 0 and run == True:
    screen.fill((255,255,255))
    menu()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            flag1 = 1
            pos = pygame.mouse.get_pos()
            k = click(pos)
    pygame.display.update()


# GENEROVANI SUDOKU
class Sudoku:
    def __init__(self,K):
        # K = pocet cisel k odstraneni
        self.K = K

        # Mrizka 9x9 plna nul 
        self.grid = np.array([[0 for x in range(9)] for y in range(9)])

    # Vygenerovani sudoku
    def fill_values(self):
        self.fill_diagonal()
        self.fill_remaining(0, 3) # prvni radek a ctvrty sloupec, protoze prvni box je jiz vyplnen
        return self.remove_digits()
        
    # Vyplni diagonalni 3x3 boxy
    def fill_diagonal(self):
        for i in range(0, 9, 3):
            self.fill_box(i, i)

    # Vyplni postupne 3x3 box
    def fill_box(self, row, col):
        num = 0
        for i in range(3):
            for j in range(3):
                while True:
                    num = random.randint(1, 9)
                    if self.not_in_box(row, col, num):
                        break
                self.grid[row + i][col + j] = num

    # Zkontroluje jestli cislo uz neni v danem 3x3 boxu
    def not_in_box(self, rowsection, colsection, num):
        for i in range(3):
            for j in range(3):
                if self.grid[rowsection + i][colsection + j] == num:
                    return False
        return True

    #  Zkontroluje jestli lze zapsat dane cislo na danou posici
    def check_valid(self, i, j, num):
        return (self.not_in_row_col(i,j,num) and self.not_in_box(i - i % 3, j - j % 3, num))

    # Zkontroluje dany radek a sloupec
    def not_in_row_col(self, i, j, num):
        for x in range(9):
            if self.grid[i][x] == num:
                return False
        for y in range(9):
            if self.grid[y][j] == num:
                return False
        return True

    # Dovyplni mrizkku
    # i = 0, j = 3
    def fill_remaining(self, i, j):
        if i == 8 and j == 9: # Konec mrizky => konec vyplnovani
            return True
        if j == 9: # Presun na dalsi radek
            i += 1
            j = 0
        if self.grid[i][j] != 0: # Preskoci zaplnena policka
            return self.fill_remaining(i, j + 1)

        # Zkusi zaplnit prazdne policko cislem
        for num in range(1, 10): # Zkusi postupne cisla 1 az 9
            #print("Test num:", num)
            if self.check_valid(i ,j, num): # Jestli je cislo vhodne
                self.grid[i][j] = num # Zaplni policko
                #print("i:", i, "j:", j, "num:", num)
                if self.fill_remaining(i, j + 1): # Presun na dalsi poicko v radku
                    return True
                self.grid[i][j] = 0 # Pokud se nepovede zaplnit dalsi policko, tak se predesle policko prepise zpet na nulu
        return False

    # Nahodne odstraneni nekterych cisel a pripraveni Sudoku k vylusteni
    def remove_digits(self):
        count = self.K
        while (count != 0):
            r = random.randint(0,8) # Nahodny radek
            c = random.randint(0,8) # Nahodny sloupec
            if (self.grid[r][c] != 0):
                count -= 1
                self.grid[r][c] = 0
            grid = self.grid
        return grid

# VYTVORENI SUDOKU A ULOZENI ORIGINALU
sudoku = Sudoku(k)
grid= sudoku.fill_values()
org_grid = grid.copy()

# LUSTENI SUDOKU

def check_solution(grid):
    # Zkontroluje jestli je kazde cislo v kazdem radku i sloupci presne jednou
    for num in range(1,10):
        for i in range(9):
            c = 0
            for j in range(9):
                if grid[i][j] == num:
                    c += 1
            if c != 1:
                return False
    c = 0
    # Zkontroluje jestli je kazde cislo v kazdem 3x3 boxu presne jednou
    for x in range(0,9,3):
        for y in range(0,9,3):
            for num in range(1,10):
                for i in range(3):
                    for j in range(3):
                        if grid[i+y][j+x] == num:
                            c+=1
                if c != 1:
                    return False
                c = 0
    return True

# GRAFIKA
screen = pygame.display.set_mode((500,600))
pygame.display.set_caption("SUDOKU")

x = 0
y = 0
dif = 500 / 9
val = 0

# Souradnice konkretniho policka
def get_cord(pos):
    if(pos[1]>500):
        return
    global x 
    x = pos[0] // dif
    global y
    y = pos[1] // dif

# Zvyrazneni zvoleneho policka
def draw_box():
    for i in range(2):
        pygame.draw.line(screen, (255, 153, 51), (x*dif-3,(y+i)*dif), (x*dif+dif+3, (y+i)*dif),7)
        pygame.draw.line(screen, (255, 153, 51), ((x+i)*dif, y*dif), ((x+i)*dif, y*dif+dif),7)

# Sudoku grid
def draw():
    for i in range(9):
        for j in range(9):
            if org_grid [i][j] != 0:
                # Zluta barva zvyrazni vyplnena policka
                pygame.draw.rect(screen,(255, 255, 75), (i*dif,j*dif,dif+1,dif+1))
                text1 = font1.render(str(org_grid[i][j]),1,(0,0,0))
                screen.blit(text1,(i*dif+20,j*dif+6))

            elif grid[i][j] != 0:
                # Tabulka s danymi cisly
                text1 = font1.render(str(grid[i][j]),1,(0,0,0))
                screen.blit(text1,(i*dif+20,j*dif+6))

    # Svisle a vodorovne cary tvorici mrizku
    for i in range(10):
        if i % 3 == 0: # zvyrazni 3x3 boxy
            thick = 7
        else:
            thick = 1
        pygame.draw.line(screen,(0,0,0),(0,i*dif),(500,i*dif),thick) #vodorovne
        pygame.draw.line(screen,(0,0,0),(i*dif,0),(i*dif,500),thick) #svisle

# Kontrola zda je zadane cislo zadano spravne
def valid (m,i,j,val):
    for it in range(9):
        if m[i][it] == val:
            return False
        if m[it][j] == val:
            return False

    it = i // 3
    jt = j // 3
    
    for i in range(it*3,it*3+3):
        for j in range(jt*3,jt*3+3):
            if m[i][j] == val:
                return False

    return True
    
run = True
flag1 = 0
flag2 = 0

while run:
    screen.fill((255,255,255))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        
        if event.type == pygame.MOUSEBUTTONDOWN:
            flag1 = 1
            pos = pygame.mouse.get_pos()
            get_cord(pos)

        if event.type == pygame.KEYDOWN:
            # pohyb sipkami
            if event.key == pygame.K_LEFT:
                if x>0:
                    x-=1
                    flag1 = 1
            if event.key == pygame.K_RIGHT:
                if x<8:
                    x+=1
                    flag1 = 1
            if event.key == pygame.K_UP:
                if y>0:
                    y-=1
                    flag1 = 1
            if event.key == pygame.K_DOWN:
                if y<8:
                    y+=1
                    flag1 = 1
            
            # zadavani cisel
            if event.key == pygame.K_1:
                val = 1
            if event.key == pygame.K_2:
                val = 2
            if event.key == pygame.K_3:
                val = 3
            if event.key == pygame.K_4:
                val = 4
            if event.key == pygame.K_5:
                val = 5
            if event.key == pygame.K_6:
                val = 6
            if event.key == pygame.K_7:
                val = 7
            if event.key == pygame.K_8:
                val = 8
            if event.key == pygame.K_9:
                val = 9

            # vymazani zadane hodnoty
            if event.key == pygame.K_BACKSPACE:
                flag2 = 1

            # obnoveni puvodni sudoku
            if event.key == pygame.K_p:
                flag2 = 0
                grid = org_grid.copy()


    if val != 0 and org_grid[int(x)][int(y)] == 0:
        grid[int(x)][int(y)] = val
        flag1 = 0
        val = 0

    if flag2 == 1 and org_grid[int(x)][int(y)] == 0:
        grid[int(x)][int(y)] = 0
        flag2 = 0

    draw()

    if flag1 == 1:
        draw_box()
        
    if 0 in grid:
        text1 = font2.render("Pro obnovení původní sudoku stiskněte klávesu 'p'.", 1, (0,0,0))
        screen.blit(text1,(text1.get_rect(center=(250,545))))
        
    if 0 not in  grid: 
        if check_solution(grid):
            text1 = font2.render("Gratuluji! Sudoku jste vyřešili správně!", 1, (0,0,0))
            screen.blit(text1,(text1.get_rect(center=(250,545))))
        else:
            text1 = font2.render("Ve vašem řešení je chyba.", 1, (0,0,0))
            screen.blit(text1,(text1.get_rect(center=(250,530))))
            text1 = font2.render("Pro obnovení původní sudoku stiskněte klávesu 'p'.", 1, (0,0,0))
            screen.blit(text1,(text1.get_rect(center=(250,570))))

    pygame.display.update()